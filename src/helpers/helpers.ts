const getFavoriteMovies = (): string[] => {
    let favoriteMovies: string[] = [];
    const data = localStorage.getItem('favoriteMovies');
    if (data) favoriteMovies = JSON.parse(data);
    return favoriteMovies;
}

const isFavoriteMovie = (id: string): boolean => {
    const favoriteMovies = getFavoriteMovies();
    return favoriteMovies.includes(id);
}

const addToFavorite = (id: string): string[] => {
    const favoriteMovies = getFavoriteMovies();
    favoriteMovies.push(id);
    localStorage.setItem('favoriteMovies', JSON.stringify(favoriteMovies));
    return favoriteMovies;
}

const removeFromFavorite = (id: string): string[] => {
    const favoriteMovies = getFavoriteMovies();
    const movieIndex = favoriteMovies.indexOf(id);
    favoriteMovies.splice(movieIndex, 1);
    localStorage.setItem('favoriteMovies', JSON.stringify(favoriteMovies));
    return favoriteMovies;
}

export { getFavoriteMovies, isFavoriteMovie, addToFavorite, removeFromFavorite };