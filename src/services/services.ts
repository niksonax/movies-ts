import { API_KEY } from "../key";
import { IMovieDefault, IMovie } from "../interfaces/interfaces";

const BASE_URL = "https://api.themoviedb.org/3/";

const getMovies = async (url: string): Promise<IMovieDefault[]> => {
    const data = await fetch (url)
                        .then((result) => result.json())
                        .then((data) => data.results)
                        .catch((error) => error);
    return  data;
}

const getMovieById = async (url: string): Promise<IMovieDefault> => {
    const data = await fetch (url)
                        .then((result) => result.json())
                        .then((data) => data)
                        .catch((error) => error);
    return  data;
}

const getMovieBackdrop = async (movieId: number): Promise<string> => {
    const url = `${BASE_URL}movie/${movieId}/images?api_key=${API_KEY}&language=en-US&include_image_language=en,pt,es,de,null`;
    const data = await fetch (url)
                        .then((result) => result.json())
                        .then((data) => data.backdrops[1].file_path)
    return data;
}

const findMovieById = async (movieId: string): Promise<IMovie> => {
    const url = `${BASE_URL}movie/${movieId}?api_key=${API_KEY}&language=en-US`;
    const movieData = await getMovieById(url);
    const movie = movieMapper(movieData);
    return movie;
}

const findMoviesById = async (moviesId: string[]): Promise<IMovie[]> => {
    const movies: IMovie[] = [];
    for (const movieId of moviesId) {
        movies.push(await findMovieById(movieId))
    }
    return movies;
}

const findMoviesByName = async (req: string, page = 1): Promise<IMovie[]> => {
    const url = `${BASE_URL}search/movie?api_key=${API_KEY}&query=${req}&page=${page}`;
    const moviesData = await getMovies(url);
    const movies = moviesMapper(moviesData);
    return movies;
}

const findPopularMovies = async (keyword: string, page = 1): Promise<IMovie[]> => {
    const url = `${BASE_URL}movie/${keyword}?api_key=${API_KEY}&page=${page}`;
    const moviesData = await getMovies(url);
    const movies = moviesMapper(moviesData);
    return movies;
}

const findRandomMovie = async (movies: IMovie[]): Promise<IMovie> => {
    const index = Math.floor(movies.length * Math.random());
    const movie = movies[index]
    const backdrop = await getMovieBackdrop(movie.id)
    movie.backdrop = backdrop;
    return movie;
} 

const moviesMapper = (moviesData: IMovieDefault[]): IMovie[] => {
    return moviesData.map(movieMapper);
}

const movieMapper = (movieData: IMovieDefault): IMovie => {
    const {id, title, overview, poster_path, release_date} = movieData;
    return { 
        id: id,
        title: title,
        overview: overview,
        poster_path: poster_path,
        release_date: release_date
    }
}

export { findMoviesByName, findPopularMovies, findRandomMovie, findMoviesById };