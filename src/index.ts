import { IMovie } from './interfaces/interfaces';
import { findMoviesByName, findPopularMovies, findRandomMovie, findMoviesById } from './services/services';
import { isFavoriteMovie, getFavoriteMovies, addToFavorite, removeFromFavorite } from './helpers/helpers';
import { buildMovieCard, buildRandomMovie, buildFavoriteMovies }  from './builders/builders';

export async function render(): Promise<void> {
    // TODO render your app here
    const searchBar =  <HTMLInputElement> document.getElementById('search');
    const submitBtn = <HTMLButtonElement> document.getElementById('submit');
    const loadMoreBtn = <HTMLButtonElement> document.getElementById('load-more');

    const btnWrapper = <HTMLDivElement> document.getElementById('button-wrapper');
    
    const movieCardsContainer = <HTMLDivElement> document.getElementById('film-container');
    const randomMovieContainer = <HTMLElement> document.getElementById('random-movie');
    const favoriteMoviesContainer = <HTMLDivElement> document.getElementById('favorite-movies');

    let favoriteHearts: NodeListOf<SVGSVGElement>;
    let pageNum = 1;
    let currentInput = 'popular';

    document.addEventListener('DOMContentLoaded', async (): Promise<void> => {
        const movies: IMovie[] = await findPopularMovies('popular');
        const randomMovie: IMovie = await findRandomMovie(movies);
        const favoriteMoviesIds: string[] = getFavoriteMovies();
        const favoriteMovies: IMovie[] = await findMoviesById(favoriteMoviesIds);
        renderRandomMovie(randomMovie);
        renderMovieCards(movies);
        renderFavoriteMoviesCards(favoriteMovies);
    })

    btnWrapper?.addEventListener('input', async (event: Event ): Promise<void> => {
        if (event.target) {
            pageNum = 1;
            currentInput = (event.target as HTMLInputElement).id;
            const movies = await findPopularMovies(currentInput);
            renderMovieCards(movies);
        }
    })

    submitBtn?.addEventListener('click', async (): Promise<void> => {
        pageNum = 1;
        currentInput = 'searchbar';
        const request = searchBar?.value;
        const movies = await findMoviesByName(request);
        renderMovieCards(movies);
    })

    loadMoreBtn.addEventListener('click', async (): Promise<void> => {
        pageNum++;
        const movies = currentInput === 'searchbar' ?
                        await findMoviesByName(searchBar?.value, pageNum) :
                        await findPopularMovies(currentInput, pageNum);
        renderMovieCards(movies, false);
    })

    type addToFavoritesHandler = (ev: Event) => void;
    const addToFavoritesHandler = async (ev: Event) => {     
        if (ev.currentTarget instanceof SVGSVGElement && ev.currentTarget.dataset.id) {
            const id = ev.currentTarget.dataset.id;
            const moviesIds = isFavoriteMovie(id) ? removeFromFavorite(id) : addToFavorite(id);
            ev.currentTarget.style.fill = isFavoriteMovie(id) ? 'red' : '#ff000078';
            const movies = await findMoviesById(moviesIds);
            renderFavoriteMoviesCards(movies);
        }
    }

    const addEventListenerList = (elements: SVGSVGElement[]): void => {
        elements.forEach((element) => element.addEventListener('click', (e)=> addToFavoritesHandler(e)));
    }   

    const renderRandomMovie = (movie: IMovie): void => {
        randomMovieContainer.innerHTML = '';
        randomMovieContainer.innerHTML += buildRandomMovie(movie.title, movie.overview);
        randomMovieContainer.style.backgroundImage = `url(https://image.tmdb.org/t/p/original/${movie.backdrop})`;
        randomMovieContainer.style.backgroundSize = 'cover';
        randomMovieContainer.style.backgroundPosition = 'center';
    }

    const renderMovieCards = (movies: IMovie[], newRender = true): void => {
        if (newRender) movieCardsContainer.innerHTML = '';
        for (const movie of movies) {
            const movieHTML = buildMovieCard(movie.poster_path, movie.id, movie.overview, movie.release_date, isFavoriteMovie(movie.id.toString()));
            movieCardsContainer.innerHTML += movieHTML;
        }
       favoriteHearts = movieCardsContainer.querySelectorAll('svg');
       addEventListenerList(Array.from(favoriteHearts));
    }

    const renderFavoriteMoviesCards = (movies: IMovie[]): void => {
        favoriteMoviesContainer.innerHTML = '';
        for (const movie of movies) {
            favoriteMoviesContainer.innerHTML += buildFavoriteMovies(movie.poster_path, movie.id, movie.overview, movie.release_date);
        }
        favoriteHearts = favoriteMoviesContainer.querySelectorAll('svg');
        addEventListenerList(Array.from(favoriteHearts));
    }
}
