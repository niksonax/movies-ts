interface IMovieDefault {
    adult: boolean,
    backdrop_path: string,
    genre_ids: number[],
    id: number,
    original_language: string,
    original_title: string,
    overview: string,
    popularity: number,
    poster_path: string,
    release_date: string,
    title: string,
    video: boolean,
    vote_average: number,
    vote_count: number
}

interface IMovie {
    id: number,
    title: string,
    overview: string,
    poster_path: string,
    release_date: string,
    backdrop?: string
}

export { IMovieDefault, IMovie }